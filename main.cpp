/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <time.h>
#include <immintrin.h> 


// Required to use intrinsic functions
//Variables para la modificacion de las componentes en el bucle for

#define VUELTAS      50
//#define CRoja   0.3
//#define CVerde  0.59
//#define CAzul  0.11

using namespace cimg_library;


// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;
//imagen de entrada
const char* SOURCE_IMG      = "uniovi_2.bmp";
//imagen de salida
const char* DESTINATION_IMG = "uni.bmp";


int main() {
	// Open file and object initialization
	
	
	CImg<> img;
	try { img.load(SOURCE_IMG); } catch (CImgException) { img.assign(); }
	
	if(!img){

		 perror("\nError en la lectura de la imagen ");
         perror("\nComprueba que los datos de entrada son correctos ");
		 exit(EXIT_FAILURE);

	}
	
	
	CImg<data_t> srcImage(SOURCE_IMG);

	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components


	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */
	struct timespec tStart, tEnd;
	double dElapsedTimeS;



	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)


	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	
	
	// Pointers to the componet arrays of the source image
		pRsrc = srcImage.data(); // pRcomp points to the R component array
		pGsrc = pRsrc + height * width; // pGcomp points to the G component array
		pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
		pRdest = pDstImage;
		pGdest = pRdest + height * width;
		pBdest = pGdest + height * width;


	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	  if (clock_gettime(CLOCK_REALTIME, &tStart)== -1)
        {
        perror("clock_gettime");
        exit(EXIT_FAILURE);
    }

	/************************************************
	 * FIXME: Algorithm.
	 * In this example, the algorithm is a components swap
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */
	

	// repetimos la operacion para que el tiempo necesario para la ejecucion oscile entre 5 y 10 segundos.
	
	
	//Variables auxiliares para poder operar en __m256
	__m256 CRoja = _mm256_set1_ps(0.3);
	__m256 CVerde = _mm256_set1_ps(0.59);
	__m256 CAzul = _mm256_set1_ps(0.11);
	
	
	data_t *AUX = (data_t *)_mm_malloc(sizeof(__m256) * 1, sizeof(__m256));
	//Reservamos memoria para una variable auxiliar que nos ayuda a guardar el valor que se modifica a cada componente
	//La reservamos de tamaño uno ya que solo necesitamos guardar un valor, el equivalente a   
	//  255-(*(pGsrc + i)*CVerde + *(pBsrc + i)*CAzul + *(pRsrc + i)*CRoja)   en la version single-thread
	
	for(uint z = 0;z <  VUELTAS ;z++){
		for (uint i = 0; i < width * height; i++){
		
					*(__m256 *)AUX=_mm256_add_ps(*(pRsrc + i)*CRoja, *(pGsrc + i)*CVerde);
					*(__m256 *)AUX=_mm256_add_ps(*(__m256 *)AUX, *(pBsrc + i)*CAzul);
					
					   *(pRdest + i) = 255 - *AUX ;  // Relleno de la componente Roja
					   
					   *(pGdest + i) = 255 - *AUX ;  // Relleno de la componente Verde
					   
					   *(pBdest + i) = 255 - *AUX ;  // Relleno de la componente Azul
					   // restamos 255 a todas las componentes para conseguir la inversa en la foto

		}
	}
	
	
	
	
	
	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	 if (clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
    {
        perror("clock_gettime");
        exit(EXIT_FAILURE);
    }
	
	printf("Finished\n");

	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

	printf("Elapsed time    : %f s.\n", dElapsedTimeS);


	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);
	//Liberamos espacio de la variable auxiliar
	free(AUX);
	return 0;
}
